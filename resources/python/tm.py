import numpy as np
import pandas as pd
import joblib
from sklearn.cluster import KMeans

def dump_model(model, path):
    joblib.dump(model, path)

def mkl(kmn, data):
    kmn.fit(data)
    dump_model(kmn, 'model.sav')

tr = pd.read_csv('transactions_.csv', sep=';') #nrows, fp
df_tr = pd.DataFrame(tr, columns=['amount', 'mcc', 'mrchname', 'cnum'])

df_tr['amount'] /= 1000
df_tr = df_tr.loc[df_tr['amount'] > 10]

km = KMeans(n_clusters=20)

dataTr = df_tr[['mcc', 'amount']].iloc[:8000, :].values
mkl(km, dataTr)
alp = km.predict(df_tr[['mcc', 'amount']].values)

df_tr['modp'] = alp
df_tr.to_csv(path_or_buf='d.csv', sep=';')

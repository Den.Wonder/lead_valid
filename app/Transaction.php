<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'purchdate', 'amount','mcc','mrchcity','mrchname','cnum','client','products',
    ];
    public function product_categories()
    {
        return $this->hasMany(ProductCategory::class);
    }

    public function client(){
        return $this->belongsTo(Client::class);
    }
}

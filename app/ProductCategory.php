<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{

    public $timestamps = false;

    protected $fillable = [
        'name',
    ];
    public function ClientCategory()
    {
        return $this->belongsTo(Client_Category::class);
    }
    public function Client()
    {
        return $this->belongsTo(Client::class);
    }
    public function Transaction()
    {
        return $this->hasMany(Transaction::class);
    }
}

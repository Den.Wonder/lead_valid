<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client_Category extends Model
{

    public $timestamps = false;
    protected $table = 'client_categories';
    protected $fillable = [
        'category','description','preferences'
    ];
    public function clients()
    {
        return $this->hasMany(Client::class);
    }
    public function products(){
        return $this->hasMany(ProductCategory::class);
    }


}

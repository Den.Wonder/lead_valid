<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Client extends Model
{
    protected $table = 'clients';
    public $timestamps = false;
    protected $fillable = [
        'cnum_','client_category','gender','age','married_','residenttype','preferences'
    ];
    public function client_categories()
    {
        return $this->belongsTo(Client_Category::class);
    }
    public function product_categories(){
        return $this->belongsToMany(ProductCategory::class);
    }

}

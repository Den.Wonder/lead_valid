<?php

namespace App\Http\Controllers;

use App\Client_Category;
use App\Client;
use App\ProductCategory;
use App\Store;
use App\Transaction;
use Illuminate\Http\Request;
use League\Csv\Reader;
use League\Csv\Statement;



class csvImports extends Controller
{



    public function import_transactions($start, $count){
        $csv = Reader::createFromPath('C:\raif_data\transactions_last_2.csv');
        $csv->setHeaderOffset(0);
        $stmt = (new Statement())
            ->offset($start)
            ->limit($count)
        ;
        $records = $stmt->process($csv);

        $number_of_categories = ProductCategory::count();

        dump('we have '.$number_of_categories.' different categories');

        $number_of_clients = Client::count();
        dump('and '.$number_of_clients.' clients, huh');








        foreach ($records as $record) {
            //do something here
            $count_of_buyings = rand(1,5);



            $client_number = rand(1,$number_of_clients);
            $client = Client::where('id','=',$client_number)->firstOrFail();

            $preferences_list = json_decode($client->preferences);

            $product_list = [];

           for($i=0;$i<$count_of_buyings;$i++){
              if($i%2==0){
                  $product_list[$i] = $preferences_list[($i/2)];
              } elseif ($i%2==1){
                  $product_list[$i] = rand(1,$number_of_categories);
              }
           }

            $product_list = json_encode($product_list);


            $str = array_values($record);
            $value = explode(';', $str[0], 6);

            if(count($value)>=5){
                $mrchname = $value[4];
            } elseif (count($value)<5){
                $ides = rand(1,10);
                $mrchname = Store::where('id','=',$ides)->first()->merchant_name;
            }

            $transaction = Transaction::create([
                'purchdate'=>$value[0],
                'amount'=>($value[1]/1000),
                'mcc'=>$value[2],
                'mrchcity'=>$value[3],
                'mrchname'=>$mrchname,
                'cnum'=>$client->cnum_,
                'client'=>$client->id,
                'products'=>$product_list,
            ]);


        }

    }

    public function import_clients($start, $count, $pref)
    {
        $csv = Reader::createFromPath('C:\raif_data\clients.csv');
        $csv->setHeaderOffset(0); //set the CSV header offset

        $csv2 = Reader::createFromPath('C:\raif_data\analysysed\9.csv');
        $csv2->setHeaderOffset(0);

        $number_of_clients = Client::count();
        $cnums = (new Statement())
            ->offset($start-$number_of_clients)
            ->limit($count)
            ;
        $client_cnums = $cnums->process($csv2);

        $ar_clients=[];
        $i=$start-$number_of_clients;
        foreach ($client_cnums as $ccnum){
            $str = array_values($ccnum);
            $value = explode(';', $str[0], 2);
            $ar_clients[$i] = $value[0];
            $i++;
        }


//get count records starting from the start'th row
        $stmt = (new Statement())
            ->offset($start)
            ->limit($count)
        ;

        $records = $stmt->process($csv);
        $pref = $pref*1;
        $number_of_categories = ProductCategory::count();




            $i=$start-$number_of_clients;
        foreach ($records as $record) {
            //do something here
            $str = array_values($record);
            $value = explode(';', $str[0], 6);



            do{
                $sec_pref = rand(1,$number_of_categories/2);
            } while($sec_pref == $pref);

            do{
                $third_pref = rand(($number_of_categories/2)+1,$number_of_categories);
            } while($third_pref == $pref);


            $customer_preferences = json_encode([
                $pref,
                $sec_pref,
                $third_pref
            ]);


            $client = Client::create([
                'cnum_' => $ar_clients[$i],
                'client_category' => $value[1],
                'gender' => $value[2],
                'age' => $value[3],
                'married_' => $value[4],
                'residenttype' => $value[5],
                'preferences'=>$customer_preferences,

            ]);
            $i++;

            dump($client);

        }

        return true;
    }




    public function import_client_categories($pref){

    $csv = Reader::createFromPath('C:\raif_data\client_categories.csv');

    $csv->setHeaderOffset(0); //set the CSV header offset

//get 25 records starting from the 0th row
    $stmt = (new Statement())
        ->offset(0)
        //->limit(25)
    ;

    $records = $stmt->process($csv);
    $pref = $pref*1;
    foreach ($records as $record) {
        //do something here
        $str = array_values($record);
        $value = explode(';',$str[0],3);



        do{
            $sec_pref = rand(1,10);
        } while($sec_pref == $pref);

        do{
            $third_pref = rand(11,20);
        } while($third_pref == $pref);


        $customer_preferences = json_encode([
            $pref,
            $sec_pref,
            $third_pref
        ]);


        //dump($customer_preferences);

        $category = Client_Category::create([
            'category'=>$value[1],
            'description'=>$value[2],
            'preferences'=>$customer_preferences,
            ]);

    }


return true;



}

    public function generate_product_categories($name){

        $category = ProductCategory::create([
            'name'=>$name,
        ]);

        return true;
    }

}

<?php

namespace App\Http\Controllers;

use App\Client;
use App\Client_Category;
use App\ProductCategory;
use App\Transaction;
use Illuminate\Http\Request;

class AnalysController extends Controller
{

    public function get_clients_id_list_by_category($category_code){



        $Current_clients = Client::where('client_category','=',$category_code)->get();

        $clients_array=[];

        $client_count = 0;

        foreach ($Current_clients as $client){
            array_push($clients_array, $client->id);
            $client_count++;
        }

        $clients_array = json_encode($clients_array);

        $category = Client_Category::where("category",'=',$category_code)->first();

        dump('В кластер клиентов '.$category->description." входит ".$client_count.' клиентов: ');

        dump($clients_array);



        return $clients_array;

    }

    public function get_current_client_preferences($client_id, $sender='frontend'){
        $product_buy_history = Transaction::where('client','=',$client_id)->get();

        $all_history = [];

        $count_of_buy = 0;

        foreach ($product_buy_history as $item) {
            $trans_prod = json_decode($item->products);
            foreach ($trans_prod as $uno){
                array_push($all_history, $uno);
                $count_of_buy++;
            }

        }



        dump("Клиент ".$client_id.' совершил '.$count_of_buy.' покупок;');



        $product_category_count = ProductCategory::count();

        $entered_count = array_count_values($all_history);

        $sorted = arsort($entered_count);

        $predict_percents = [];

        if($sender == 'frontend'){


        for ($i=1;$i<=$product_category_count;$i++){
            $predict_percents[$i] = round((($entered_count[$i]/$count_of_buy)*100), 2);
        }
        $sorted_again = arsort($predict_percents);
       // dump($entered_count);

            dump("Вероятность будущей покупки по категориям (в процентах): ");
            dump($predict_percents);
        }

        $prefer_ids = array_keys($entered_count);





        if($sender == 'frontend'){
            return $predict_percents;
        } else {
            return $entered_count;
        }

    }

    public function regenerate_clients_cluster_preferences($client_category){
            $clients = json_decode($this->get_clients_id_list_by_category($client_category));


             $category = Client_Category::where("category",'=',$client_category)->first();

            $count_of_entered_summary = [];
            $i = 0;

           // dump($clients);

            $clients_count = Client::count();

            $clients_part = round((count($clients)/$clients_count)*100,2);
          //  dump($clients_part);

            foreach($clients as $client){
                $count_of_entered_summary[$i] = $this->get_current_client_preferences($client, 'backend');
                $sort = ksort($count_of_entered_summary[$i]);
                $i++;
            }

           // dump($count_of_entered_summary);


            $count_of_buy = 0;
            $all_history = [];

           // $sort= asort($count_of_entered_summary);


        $product_category_count = ProductCategory::count();

        $final_ord = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];

        foreach ($count_of_entered_summary as $item) {
            for($i=1;$i<=$product_category_count;$i++){
                if(array_key_exists($i, $item)){
                    $final_ord[$i-1] += $item[$i];
                    $count_of_buy += $item[$i];
                }
            }
        }

       // dump($final_ord);
        dump('Все клиенты из категории '.$category->description." совершили:");
        dump($count_of_buy);
        dump("покупок");

        $final_percent_present = [];

        for($i=0;$i<$product_category_count;$i++){
            $final_percent_present[$i] = round(($final_ord[$i]/$count_of_buy)*100,2);
        }

        $soring = arsort($final_percent_present);


        dump("Вероятность будущих покупок клиентами из категории ".$category->description." (в процентном соотношении:)");
        dump($final_percent_present);

       // dump($count_of_buy);
       // dump($all_history);


    }
}

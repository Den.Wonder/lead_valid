<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/get_clients_by_categories/{category_code}','AnalysController@get_clients_id_list_by_category');

Route::get('/get_clients_buy_history/{client_id}','AnalysController@get_current_client_preferences');

Route::get('/regenerate_clients_cluster_preferences/{category_code}','AnalysController@regenerate_clients_cluster_preferences');

Route::get('/import_transactions/{start}/{count}', 'csvImports@import_transactions');
Route::get('/create_product/{name}','csvImports@generate_product_categories');
Route::get('/import_cc/{pref}','csvImports@import_client_categories');
Route::get('/import_c/{start}/{count}/{pref}','csvImports@import_clients');
Route::post('import','csvImport@parseImport');

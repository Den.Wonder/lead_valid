<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->string('cnum_')->nullable();


            $table->integer('client_category')->nullable()->unsigned();

            $table->string('gender')->nullable();
            $table->integer('age')->nullable();
            $table->string('married_')->nullable();
            $table->string('residenttype')->nullable();
            $table->json('preferences')->nullable();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}

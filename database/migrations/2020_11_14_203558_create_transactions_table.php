<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->dateTime('purchdate')->nullable();
            $table->integer('amount')->nullable();
            $table->integer('mcc')->nullable();
            $table->string('mrchcity')->nullable();
            $table->string('mrchname')->nullable();
            $table->string('cnum')->nullable();
            $table->integer('client')->nullable();
            $table->json('products')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
